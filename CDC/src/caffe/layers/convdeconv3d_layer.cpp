/*
 *
 *  When: 2016/09/04
 *  Who: Zheng Shou
 *  Where: Columbia University
 *
 */

#include "caffe/layer.hpp"
#include "caffe/convdeconv3d_layer.hpp"
#include "caffe/util/vol2col.hpp"
#include "caffe/util/swap_blob_array_channel_length.hpp"
#include "caffe/filler.hpp"
#include "caffe/util/math_functions.hpp"
#include <vector>

namespace caffe {

template <typename Dtype>
void Convdeconv3DLayer<Dtype>::SetUp(const vector<Blob<Dtype>*>& bottom,
      vector<Blob<Dtype>*>* top) {

  CHECK_EQ(bottom.size(), 1) << "Convdeconv Layer takes a single blob as input.";
  CHECK_EQ(top->size(), 1) << "Convdeconv Layer takes a single blob as output.";

  kernel_size_ = this->layer_param_.convolution_param().kernel_size();
  kernel_depth_ = this->layer_param_.convolution_param().kernel_depth();
  stride_ = this->layer_param_.convolution_param().stride();
  temporal_stride_ = this->layer_param_.convolution_param().temporal_stride();
  pad_ = this->layer_param_.convolution_param().pad();
  temporal_pad_ = this->layer_param_.convolution_param().temporal_pad();
  num_ = bottom[0]->num();
  channels_ = bottom[0]->channels();
  length_ = bottom[0]->length();
  height_ = bottom[0]->height();
  width_ = bottom[0]->width();
  num_output_ = this->layer_param_.convolution_param().num_output();
  CHECK_GT(num_output_, 0);


  // The vol2col result buffer would only hold one image at a time to avoid
  // overly large memory usage.

  length_out_ = (length_ - 1) * temporal_stride_ - 2 * temporal_pad_ + kernel_depth_;	// true output
  height_out_ = (height_ + 2 * pad_ - kernel_size_) / stride_ + 1;
  width_out_ = (width_ + 2 * pad_ - kernel_size_) / stride_ + 1;

  // buffer - output (diff) of vol2col and input (data) to col2vol
  col_buffer_.Reshape(
      1, num_output_ * kernel_depth_, length_, 1, 1);	// dim is num_output_ * kernel_depth_ * length_ . col_buffer_ is blob type, so contain data and diff of same dim. 
	  
  // buffer - output (diff) of swap_channel_length and input (data) to col2vol
  swap_buffer_.Reshape(
      1, length_, channels_, kernel_size_, kernel_size_);	// dim is (1,IL,IC,KH=IH,KW=IW)


  bias_term_ = this->layer_param_.convolution_param().bias_term();

  // Figure out the dimensions for individual gemms. as explained in the above.
  M_ = num_output_;
  K_ = channels_ * length_ * kernel_size_ * kernel_size_;
  N_ = 1 * height_out_ * width_out_;	// #times of sliding
  M0_ = num_output_;
  N0_ = length_out_ * height_out_ * width_out_;	// should be length_out_*1*1

  // output size
  (*top)[0]->Reshape(bottom[0]->num(), num_output_, length_out_, height_out_, width_out_);	// true output

  // Check if we need to set up the weights
  if (this->blobs_.size() > 0) {
    LOG(INFO) << "Skipping parameter initialization";
  } else {
    if (bias_term_) {
      this->blobs_.resize(2);
    } else {
      this->blobs_.resize(1);
    }
    // Initialize the weights
    this->blobs_[0].reset(new Blob<Dtype>(
        num_output_, kernel_depth_, channels_, kernel_size_, kernel_size_));	// dim of weight for conv-de-conv. switch the position for length and channel to fit computation in the following using caffe_cpu_gemm function.
    // fill the weights
    shared_ptr<Filler<Dtype> > weight_filler(GetFiller<Dtype>(
        this->layer_param_.convolution_param().weight_filler()));
    weight_filler->Fill(this->blobs_[0].get());
    // If necessary, initialize and fill the bias term
    if (bias_term_) {
      this->blobs_[1].reset(new Blob<Dtype>(1, 1, 1, 1, num_output_));	// dim of bias = true num_output_. each output channel shares one bias.
      shared_ptr<Filler<Dtype> > bias_filler(GetFiller<Dtype>(
          this->layer_param_.convolution_param().bias_filler()));
      bias_filler->Fill(this->blobs_[1].get());
    }


  }

  // Set up the bias filler. bias_multiplier_(simly all one, dim=N0_=OL*OH*OW) is used for multiplying with delta for updating bias.
  if (bias_term_) {
    bias_multiplier_.reset(new SyncedMemory(N0_ * sizeof(Dtype)));
    Dtype* bias_multiplier_data =
        reinterpret_cast<Dtype*>(bias_multiplier_->mutable_cpu_data());
    for (int i = 0; i < N0_; ++i) {
        bias_multiplier_data[i] = 1.;
    }
  }
}

template <typename Dtype>
Dtype Convdeconv3DLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      vector<Blob<Dtype>*>* top) {
		  
  const Dtype* bottom_data = bottom[0]->cpu_data();
  Dtype* top_data = (*top)[0]->mutable_cpu_data();
  Dtype* col_data = col_buffer_.mutable_cpu_data();
  Dtype* swap_data = swap_buffer_.mutable_cpu_data();
  const Dtype* weight = this->blobs_[0]->cpu_data();		  
	  
  for (int n = 0; n < num_; ++n) {
	  
	// bottom_data (1,c,IL,IH=KW,IW=KW) -> swap_data (1,IL,c,IH=KW,IW=KW)
	swap_blob_array_channel_length_cpu(bottom_data + bottom[0]->offset(n), channels_, length_, height_, width_, swap_data);
	
	// col_data (1, num_output_*KL, IL, 1, 1) = (num_output_*KL)*(IL)
	// weight (num_output_,KL,c,KH,KW)=(num_output_*KL)*(c*KH*KW)
	// x 
	// swap_data (1,IL,c,IH=KW,IW=KW)=(IL)*(c*KH*KW)->Trans->(c*KH*KW)*(IL)
    caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasTrans, num_output_*kernel_depth_, length_, channels_*kernel_size_*kernel_size_,
    (Dtype)1., weight, swap_data,
    (Dtype)0., col_data);

    // col2vol from col_data (1,num_output_*KL,IL,1,1) -> top_data (num_output_*OL)
	// treat as deconv with size=1 in H and W for both input and output. 
    col2vol_cpu(col_data, num_output_, length_out_, 1, 1,
    1, kernel_depth_, 0, temporal_pad_, 1, temporal_stride_,
    top_data + (*top)[0]->offset(n));

    // third, add bias
    if (bias_term_) {
      caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, num_output_,
      N0_, 1, (Dtype)1., this->blobs_[1]->cpu_data(),
      reinterpret_cast<const Dtype*>(bias_multiplier_->cpu_data()),
      (Dtype)1., top_data + (*top)[0]->offset(n));
    }
  }
	  return Dtype(0.);
}

template <typename Dtype>
void Convdeconv3DLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
      const bool propagate_down, vector<Blob<Dtype>*>* bottom) {
		  
  const Dtype* top_diff = top[0]->cpu_diff();
  const Dtype* weight = this->blobs_[0]->cpu_data();
  Dtype* weight_diff = this->blobs_[0]->mutable_cpu_diff();
  const Dtype* bottom_data = (*bottom)[0]->cpu_data();
  Dtype* bottom_diff = (*bottom)[0]->mutable_cpu_diff();
  Dtype* col_data = col_buffer_.mutable_cpu_data();
  Dtype* col_diff = col_buffer_.mutable_cpu_diff();
  Dtype* swap_data = swap_buffer_.mutable_cpu_data();
  Dtype* swap_diff = swap_buffer_.mutable_cpu_diff();
  // bias gradient if necessary
  Dtype* bias_diff = NULL;

  if (bias_term_) {
    bias_diff = this->blobs_[1]->mutable_cpu_diff();
    memset(bias_diff, 0, sizeof(Dtype) * this->blobs_[1]->count());
    for (int n = 0; n < num_; ++n) {
	  caffe_cpu_gemv<Dtype>(CblasNoTrans, num_output_, N0_,
		  1., top_diff + top[0]->offset(n),
		  reinterpret_cast<const Dtype*>(bias_multiplier_->cpu_data()), 1.,
		  bias_diff);
    }
  }
		  
  memset(weight_diff, 0, sizeof(Dtype) * this->blobs_[0]->count());
  for (int n = 0; n < num_; ++n) {
	// since we saved memory in the forward pass by not storing all swap data,
	// we will need to recompute them.
	
	// bottom_data (1,c,IL,IH=KW,IW=KW) -> swap_data (1,IL,c,IH=KW,IW=KW)
	swap_blob_array_channel_length_cpu(bottom_data + (*bottom)[0]->offset(n), channels_, length_, height_, width_, swap_data);

	// gradient w.r.t. weight. Note that we will accumulate diffs.
	
	// vol2col from top_diff (num_output_*OL) -> col_diff (1,num_output_*KL,IL,1,1)
	// treat as deconv with size=1 in H and W for both input and output.
	vol2col_cpu(top_diff + top[0]->offset(n), num_output_, length_out_, 1, 1,
	  	    1, kernel_depth_, 0, temporal_pad_, 1, temporal_stride_, col_diff);

	// weight_diff (num_output_,KL,c,KH,KW)=(num_output_*KL)*(c*KH*KW)
	// col_diff (1, num_output_*KL, IL, 1, 1) = (num_output_*KL)*(IL)
	// x 
	// swap_data (1,IL,c,IH=KW,IW=KW)=(IL)*(c*KH*KW)
	caffe_cpu_gemm<Dtype>(CblasNoTrans, CblasNoTrans, num_output_*kernel_depth_, channels_*kernel_size_*kernel_size_, length_,
				  (Dtype)1., col_diff,
				  swap_data, (Dtype)1.,
				  weight_diff);

	  // gradient w.r.t. bottom data, if necessary
	  if (propagate_down) {
		  // swap_diff = col_diff x weight
		  // swap_diff (1,IL,c,IH=KW,IW=KW)=(IL)*(c*KH*KW)
		  // col_diff (1, num_output_*KL, IL, 1, 1) = (num_output_*KL)*(IL)->Trans->(IL)*(num_output_*KL)
		  // x 
		  // weight (num_output_,KL,c,KH,KW)=(num_output_*KL)*(c*KH*KW)
		  caffe_cpu_gemm<Dtype>(CblasTrans, CblasNoTrans, length_, channels_*kernel_size_*kernel_size_, num_output_*kernel_depth_,
				  (Dtype)1., col_diff, weight,
				  (Dtype)0., swap_diff);
				  
		  // swap_diff (1,IL,c,IH=KW,IW=KW) -> bottom_diff (1,c,IL,IH=KW,IW=KW)
		  swap_blob_array_channel_length_cpu(swap_diff, length_, channels_, height_, width_, bottom_diff + (*bottom)[0]->offset(n));
	  }

  }
  
}
	  
INSTANTIATE_CLASS(Convdeconv3DLayer);

}  // namespace caffe
