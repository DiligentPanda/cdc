/*
 *
 *  When: 2016/09/03
 *  Who: Zheng Shou
 *  Where: Columbia University
 *
 */

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <cstring>

#include "caffe/common.hpp"
#include "caffe/util/swap_blob_array_channel_length.hpp"

namespace caffe {


template <typename Dtype>
__global__ void swap_blob_array_channel_length_gpu_kernel(const int n, const Dtype* data_in,
    const int channels, const int length, const int height, const int width, Dtype* data_out) {
  CUDA_KERNEL_LOOP(index, n) {
    int index_in = index;
    int w = index % width;
    int h = (index / width ) % height;
    int l = (index / width / height) % length;
    int c = index / width / height / length;
    int index_out = ((l * channels + c) * height + h) * width + w;
	
	data_out += index_out;
    *data_out = data_in[ index_in ];
	
  }
}

template <typename Dtype>
void swap_blob_array_channel_length_gpu(const Dtype* data_in, const int channels, const int length,
    const int height, const int width, Dtype* data_out) {
  // We are going to launch channels * length * height * width kernels, each
  // kernel responsible for copying a single-channel grid.
  int num_kernels = channels * length * height * width;
  // NOLINT_NEXT_LINE(whitespace/operators)
  swap_blob_array_channel_length_gpu_kernel<Dtype><<<CAFFE_GET_BLOCKS(num_kernels),
                             CAFFE_CUDA_NUM_THREADS>>>(
      num_kernels, data_in, channels, length, height, width, data_out);
  CUDA_POST_KERNEL_CHECK;
}


// Explicit instantiation
template void swap_blob_array_channel_length_gpu<float>(const float* data_in, const int channels, const int length,
    const int height, const int width, float* data_out);
template void swap_blob_array_channel_length_gpu<double>(const double* data_in, const int channels, const int length,
    const int height, const int width, double* data_out);

}  // namespace caffe
